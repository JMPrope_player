#ifndef BAREBUTTON_H
#define BAREBUTTON_H

class BareButton {
private:
	uint8_t _pin;
public:
	BareButton(uint8_t pin) {
		_pin = pin;
		pinMode(_pin, INPUT_PULLUP);
	}

	boolean isPressed() {
		return !digitalRead(_pin); // pin low -> pressed
	}
};

#endif // BAREBUTTON_H
