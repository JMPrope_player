#include <FastLED.h>
#include <FastLED_animation.h>

#include "BareButton.h"

//#include "animations/Adafruit.h"
//#include "animations/Debian.h"
//#include "animations/Firefox.h"
#include "animations/JMPrope.h"
//#include "animations/Openhardware.h"
//#include "animations/Opensource.h"
//#include "animations/S.S.C.Napoli.h"
//#include "animations/ao2.h"
//#include "animations/ao2it.h"

#define DATA_PIN 0
#define INPUT_PIN 2

/* This is the same exposure time to set up on the camera */
#define EXPOSURE_TIME_MSEC 500UL

static struct CRGB leds[NUM_LEDS];

static BareButton button = BareButton(INPUT_PIN);
static unsigned long frame_delay_usecs = EXPOSURE_TIME_MSEC * 1000UL / NUM_FRAMES;

void setup()
{
	LEDS.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
	unsigned long remaining;
	unsigned long elapsed = 0;

	if (button.isPressed()) {
		unsigned long t0 = micros();
		animation.draw(leds);
		elapsed = micros() - t0;
	} else {
		for (int i = 0; i < NUM_LEDS; i++)
			leds[i] = CRGB(0, 0, 0);
		LEDS.show();
		animation.reset();
	}
	remaining = frame_delay_usecs - elapsed;
#if 0
	/* use leds to show how many _milliseconds_ we will have to wait to
	 * achieve a constant framerate */
	for (unsigned int i = 0; i < NUM_LEDS; i++)
		leds[i] = CRGB(0, 0, 0);
	for (unsigned int i = 0; i < remaining / 1000; i++)
		leds[i] = CRGB(255, 0, 0);
	LEDS.show();
	delay(100);
#else
	delayMicroseconds(remaining);
#endif
}
