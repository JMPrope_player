ARDUINO_VAR_PATH := $(HOME)/sketchbook/hardware/attiny/variants
BOARDS_TXT := $(HOME)/sketchbook/hardware/attiny/boards.txt
BOARD_TAG := trinket3
ISP_PROG := usbtiny
AVRDUDE_OPTS = -q

ARDUINO_LIBS = FastLED FastLED_animation

-include $(HOME)/.arduino/Arduino.mk.inc
include /usr/share/arduino/Arduino.mk

upload: $(TARGET_HEX) verify_size
	$(MAKE) ispload
