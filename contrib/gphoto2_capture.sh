#!/bin/sh
#
# gphoto2_capture.sh - Do automated high exposure tethered photo shoots
#
# Copyright (C) 2014  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

# Get the configuration variables with these commands:
# gphoto2 --set-config capture=1
# gphoto2 --list-all-config | sed -e 's/^[^\/]/\t\0/g' > Camera-config.txt
#

CAPTURE_MODE=$(LANG=C gphoto2 --get-config capture | grep Current | cut -d ' ' -f 2 | tr -d '\n')

if [ "x$CAPTURE_MODE" = "x" ];
then
  exit 1
fi

if [ $CAPTURE_MODE -eq 0 ];
then
  gphoto2 --set-config capture=1
fi

# The settings below are for a Canon powershot A85, with these settings:
#   - Save pictures on the memory card
#   - F-number 2.8
#   - Manual shooting mode
#   - Exposure 0.5 seconds
#   - Superfine quality
#   - ISO speed 100

gphoto2 \
  -I 1 \
  --set-config-index capturetarget=1 \
  --set-config-index /main/capturesettings/aperture=0 \
  --set-config-index /main/capturesettings/shootingmode=3 \
  --set-config-index /main/capturesettings/shutterspeed=15 \
  --set-config-index /main/imgsettings/imagequality=2 \
  --set-config-index /main/imgsettings/iso=1 \
  --capture-image \
  "$@"

# Stop the capture with Ctrl-C and then download all the files on the PC,
# after that delete them from the camera:
# gphoto2 --get-all-files
# gphoto2 --delete-all-files --recurse
