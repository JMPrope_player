const PROGMEM prog_uint8_t animationData[]  = {
// Length of the color table - 1, in bytes. length: 1 byte
   3,
// Color table section. Each entry is 3 bytes. length: 12 bytes
  48, 169, 255,
   0,   0,   0,
 255, 255, 255,
   0,  60, 231,
// Pixel runs section. Each pixel run is 2 bytes. length: -1 bytes
   1,   2,
   1,   1,
  19,   0,
   1,   2,
   8,   3,
   2,   1,
  19,   0,
   1,   2,
   8,   3,
   1,   1,
  20,   0,
   1,   2,
   8,   3,
   1,   1,
  20,   0,
   1,   2,
   8,   3,
   1,   1,
  20,   0,
   1,   2,
   8,   3,
   1,   1,
  10,   0,
   1,   1,
   9,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   3,   1,
   9,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   1,   1,
   2,   2,
   1,   1,
   8,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   1,   1,
   2,   2,
   1,   1,
   8,   0,
   1,   2,
   8,   3,
   1,   1,
   9,   0,
   1,   1,
   1,   2,
   1,   1,
   8,   0,
   1,   2,
   8,   3,
   1,   1,
   9,   0,
   1,   1,
   2,   2,
   1,   1,
   7,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   1,   1,
   4,   2,
   1,   1,
   6,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   1,   1,
   5,   2,
   1,   1,
   5,   0,
   1,   2,
   8,   3,
   1,   1,
   8,   0,
   1,   1,
   6,   2,
   1,   1,
   4,   0,
   1,   2,
   8,   3,
   1,   1,
   7,   0,
   1,   1,
   3,   2,
   3,   1,
   1,   2,
   1,   1,
   4,   0,
   1,   2,
   8,   3,
   1,   1,
   7,   0,
   1,   1,
   2,   2,
   1,   1,
   3,   0,
   2,   1,
   4,   0,
   1,   2,
   8,   3,
   1,   1,
   6,   0,
   1,   1,
   2,   2,
   1,   1,
  10,   0,
   1,   2,
   8,   3,
   1,   1,
   6,   0,
   1,   1,
   2,   2,
   1,   1,
   7,   0,
   3,   3,
   1,   2,
   8,   3,
   1,   1,
   6,   0,
   1,   1,
   1,   2,
   1,   1,
   4,   0,
   7,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   2,   2,
   1,   1,
   2,   0,
   9,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   8,   3,
   1,   1,
   5,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   5,   3,
   2,   0,
   1,   3,
   1,   1,
   5,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   3,   3,
   4,   0,
   1,   3,
   1,   1,
   5,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   1,   3,
   6,   0,
   1,   3,
   1,   2,
   1,   1,
   4,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   7,   0,
   1,   3,
   1,   2,
   1,   1,
   4,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   3,
   1,   2,
   7,   0,
   1,   3,
   1,   2,
   1,   1,
   4,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   7,   0,
   1,   3,
   1,   2,
   2,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   7,   0,
   1,   3,
   1,   2,
   2,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   7,   0,
   1,   3,
   2,   2,
   2,   1,
   2,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   3,
   1,   2,
   7,   0,
   1,   3,
   3,   2,
   1,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  10,   3,
   1,   2,
   7,   0,
   1,   3,
   3,   2,
   3,   1,
   1,   0,
   1,   1,
   2,   2,
   1,   1,
  10,   3,
   1,   2,
   7,   0,
   1,   3,
   5,   2,
   4,   1,
   2,   2,
   1,   1,
   9,   3,
   1,   2,
   7,   0,
   1,   3,
   7,   2,
   2,   1,
   2,   2,
   6,   1,
   4,   3,
   1,   2,
   7,   0,
   1,   3,
  16,   2,
   2,   1,
   3,   3,
   1,   2,
   7,   0,
   1,   3,
  15,   2,
   1,   1,
   5,   3,
   1,   2,
   7,   0,
   1,   3,
  14,   2,
   1,   1,
   6,   3,
   1,   2,
   7,   0,
   1,   3,
  14,   2,
   1,   1,
   6,   3,
   1,   2,
   7,   0,
   1,   3,
  13,   2,
   1,   1,
   7,   3,
   1,   2,
   7,   0,
   1,   3,
  12,   2,
   1,   1,
   8,   3,
   1,   2,
   7,   0,
   1,   3,
  12,   2,
   1,   1,
   8,   3,
   1,   2,
   7,   0,
   1,   3,
  10,   2,
   2,   1,
   9,   3,
   1,   2,
   7,   0,
   1,   3,
   8,   2,
   2,   1,
  11,   3,
   1,   2,
   7,   0,
   1,   3,
   7,   2,
   2,   1,
  12,   3,
   1,   2,
   7,   0,
   1,   3,
   6,   2,
   1,   1,
  14,   3,
   1,   2,
   7,   0,
   1,   3,
   5,   2,
   1,   1,
  15,   3,
   1,   2,
   7,   0,
   1,   3,
   4,   2,
   2,   1,
  15,   3,
   1,   2,
   1,   3,
   6,   0,
   1,   3,
   4,   2,
   1,   1,
   1,   0,
  15,   3,
   1,   2,
   3,   3,
   4,   0,
   1,   3,
   4,   2,
   1,   1,
   1,   0,
  15,   3,
   1,   2,
   4,   3,
   3,   0,
   1,   3,
   3,   2,
   1,   1,
   3,   0,
  14,   3,
   1,   2,
   6,   3,
   1,   0,
   1,   3,
   3,   2,
   1,   1,
   4,   0,
  13,   3,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   4,   0,
  13,   3,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   6,   0,
  11,   3,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   6,   0,
   3,   1,
   9,   3,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   6,   0,
   1,   1,
   2,   2,
   1,   1,
   8,   3,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   7,   0,
   1,   1,
   1,   2,
   1,   1,
   4,   0,
   4,   3,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   7,   0,
   1,   1,
   2,   2,
   1,   1,
   7,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   7,   0,
   1,   1,
   2,   2,
   1,   1,
   7,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   7,   0,
   1,   1,
   3,   2,
   1,   1,
   6,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   6,   0,
   1,   1,
   5,   2,
   1,   1,
   5,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   6,   0,
   1,   1,
   6,   2,
   1,   1,
   4,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   5,   0,
   1,   1,
   3,   2,
   3,   1,
   2,   2,
   1,   1,
   3,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   5,   0,
   1,   1,
   2,   2,
   1,   1,
   3,   0,
   2,   1,
   4,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   4,   0,
   1,   1,
   2,   2,
   1,   1,
  10,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   4,   0,
   1,   1,
   2,   2,
   1,   1,
  10,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   4,   0,
   1,   1,
   1,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   3,   0,
   1,   1,
   2,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
   3,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   2,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   2,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   2,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   3,   2,
   1,   1,
   2,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   4,   2,
   1,   1,
   1,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   4,   2,
   1,   1,
   1,   0,
   1,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   4,   2,
   3,   1,
   1,   2,
   1,   1,
  12,   0,
   1,   2,
   8,   3,
   5,   2,
   2,   1,
   2,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   6,   2,
   1,   1,
   2,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   9,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
   9,   2,
   1,   1,
  11,   0,
   1,   2,
   8,   3,
  10,   2,
   1,   1,
  10,   0,
   1,   2,
   8,   3,
  10,   2,
   1,   1,
  10,   0,
   1,   2,
   8,   3,
  11,   2,
   1,   1,
   9,   0,
   1,   2,
   8,   3,
  12,   2,
   1,   1,
   8,   0,
   1,   2,
   8,   3,
  12,   2,
   2,   1,
   7,   0,
   1,   2,
   8,   3,
  13,   2,
   2,   1,
   6,   0,
   1,   2,
   8,   3,
  14,   2,
   1,   1,
   6,   0,
   1,   2,
   8,   3,
  13,   2,
   1,   1,
   7,   0,
   1,   2,
   8,   3,
   8,   2,
   5,   1,
   8,   0,
   1,   2,
   8,   3,
   5,   2,
   4,   1,
  12,   0,
   1,   2,
   8,   3,
   4,   2,
   2,   1,
  15,   0,
   1,   2,
   8,   3,
   3,   2,
   2,   1,
  16,   0,
   1,   2,
   8,   3,
   2,   2,
   2,   1,
  17,   0,
   1,   2,
   8,   3,
   2,   2,
   1,   1,
  18,   0,
   1,   2,
   8,   3,
   1,   2,
   2,   1,
  18,   0,
   1,   2,
   8,   3,
   1,   2,
   1,   1,
  19,   0,
   1,   2,
   8,   3,
   1,   2,
   1,   1,
  19,   0,
   1,   2,
   8,   3,
};

#define NUM_FRAMES 101
#define NUM_LEDS 30
Animation animation(NUM_FRAMES, animationData, NUM_LEDS);
