const PROGMEM prog_uint8_t animationData[]  = {
// Length of the color table - 1, in bytes. length: 1 byte
   1,
// Color table section. Each entry is 3 bytes. length: 6 bytes
 213,   7,  78,
   0,   1,   0,
// Pixel runs section. Each pixel run is 2 bytes. length: -1 bytes
   3,   1,
   2,   0,
   1,   1,
   1,   0,
  23,   1,
   3,   1,
   2,   0,
   1,   1,
   1,   0,
  21,   1,
   1,   0,
   1,   1,
   3,   1,
   2,   0,
  22,   1,
   2,   0,
   1,   1,
   2,   1,
   2,   0,
   1,   1,
   1,   0,
  20,   1,
   2,   0,
   2,   1,
   2,   1,
   2,   0,
   1,   1,
   1,   0,
  20,   1,
   2,   0,
   2,   1,
   2,   1,
   2,   0,
  21,   1,
   3,   0,
   2,   1,
   2,   1,
   2,   0,
   1,   1,
   1,   0,
  18,   1,
   3,   0,
   3,   1,
   2,   1,
   2,   0,
  20,   1,
   3,   0,
   3,   1,
   2,   1,
   2,   0,
  19,   1,
   3,   0,
   4,   1,
   2,   1,
   2,   0,
  19,   1,
   3,   0,
   4,   1,
   2,   1,
   1,   0,
  19,   1,
   3,   0,
   5,   1,
   2,   1,
   1,   0,
  19,   1,
   3,   0,
   5,   1,
   1,   1,
   2,   0,
  18,   1,
   4,   0,
   5,   1,
   1,   1,
   2,   0,
   2,   1,
   1,   0,
  14,   1,
   4,   0,
   6,   1,
   1,   1,
   2,   0,
   2,   1,
   1,   0,
  14,   1,
   4,   0,
   6,   1,
   1,   1,
   2,   0,
   2,   1,
   1,   0,
  14,   1,
   4,   0,
   6,   1,
   1,   1,
   2,   0,
   2,   1,
   1,   0,
  13,   1,
   5,   0,
   6,   1,
   1,   1,
   2,   0,
   2,   1,
   1,   0,
  13,   1,
   5,   0,
   6,   1,
   1,   1,
   2,   0,
  17,   1,
   3,   0,
   7,   1,
   1,   1,
   2,   0,
  17,   1,
   3,   0,
   7,   1,
   1,   1,
   2,   0,
  17,   1,
   3,   0,
   7,   1,
   1,   1,
   2,   0,
  17,   1,
   3,   0,
   7,   1,
   1,   1,
   2,   0,
  16,   1,
   4,   0,
   7,   1,
   1,   1,
   3,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   3,   0,
   7,   1,
   2,   1,
   2,   0,
  16,   1,
   4,   0,
   6,   1,
   2,   1,
   2,   0,
  16,   1,
   4,   0,
   6,   1,
   2,   1,
   3,   0,
  16,   1,
   3,   0,
   6,   1,
   2,   1,
   3,   0,
  15,   1,
   4,   0,
   6,   1,
   3,   1,
   2,   0,
  15,   1,
   5,   0,
   5,   1,
   3,   1,
   2,   0,
  16,   1,
   4,   0,
   5,   1,
   3,   1,
   3,   0,
  15,   1,
   4,   0,
   5,   1,
   4,   1,
   2,   0,
  15,   1,
   4,   0,
   5,   1,
   4,   1,
   2,   0,
  15,   1,
   4,   0,
   5,   1,
   4,   1,
   2,   0,
  15,   1,
   5,   0,
   4,   1,
   5,   1,
   1,   0,
  15,   1,
   5,   0,
   4,   1,
   5,   1,
   2,   0,
  14,   1,
   6,   0,
   3,   1,
   5,   1,
   2,   0,
  14,   1,
   6,   0,
   3,   1,
   6,   1,
   2,   0,
  13,   1,
   6,   0,
   3,   1,
   6,   1,
   2,   0,
  14,   1,
   5,   0,
   3,   1,
   7,   1,
   1,   0,
  14,   1,
   6,   0,
   2,   1,
   7,   1,
   2,   0,
  13,   1,
   6,   0,
   2,   1,
   8,   1,
   1,   0,
  13,   1,
   6,   0,
   2,   1,
   8,   1,
   2,   0,
  12,   1,
   7,   0,
   1,   1,
   9,   1,
   1,   0,
  13,   1,
   6,   0,
   1,   1,
   9,   1,
   2,   0,
  12,   1,
   6,   0,
   1,   1,
  10,   1,
   1,   0,
  12,   1,
   5,   0,
   2,   1,
  10,   1,
   2,   0,
  11,   1,
   5,   0,
   2,   1,
  10,   1,
   2,   0,
  11,   1,
   5,   0,
   2,   1,
  11,   1,
   1,   0,
  12,   1,
   5,   0,
   1,   1,
  11,   1,
   2,   0,
  11,   1,
   5,   0,
   1,   1,
  12,   1,
   1,   0,
  11,   1,
   4,   0,
   2,   1,
  12,   1,
   1,   0,
  11,   1,
   4,   0,
   2,   1,
  12,   1,
   1,   0,
  11,   1,
   5,   0,
   1,   1,
  12,   1,
   1,   0,
  11,   1,
   5,   0,
   1,   1,
  23,   1,
   6,   0,
   1,   1,
  23,   1,
   6,   0,
   1,   1,
  23,   1,
   6,   0,
   1,   1,
  22,   1,
   7,   0,
   1,   1,
  22,   1,
   7,   0,
   1,   1,
  22,   1,
   6,   0,
   2,   1,
  21,   1,
   7,   0,
   2,   1,
  21,   1,
   3,   0,
   2,   1,
   1,   0,
   3,   1,
  20,   1,
   5,   0,
   5,   1,
  20,   1,
   3,   0,
   7,   1,
  11,   1,
   1,   0,
   7,   1,
   4,   0,
   7,   1,
  19,   1,
   4,   0,
   7,   1,
  18,   1,
   5,   0,
   7,   1,
  18,   1,
   4,   0,
   8,   1,
  18,   1,
   3,   0,
   9,   1,
  17,   1,
   3,   0,
  10,   1,
  17,   1,
   2,   0,
  11,   1,
  15,   1,
   4,   0,
  11,   1,
  15,   1,
   3,   0,
  12,   1,
  15,   1,
   2,   0,
  13,   1,
  14,   1,
   3,   0,
  13,   1,
   4,   1,
   1,   0,
   8,   1,
   3,   0,
  14,   1,
   3,   1,
   2,   0,
   8,   1,
   2,   0,
  15,   1,
  12,   1,
   3,   0,
  15,   1,
  11,   1,
   3,   0,
  16,   1,
  11,   1,
   2,   0,
  17,   1,
  10,   1,
   3,   0,
  17,   1,
   9,   1,
   3,   0,
  18,   1,
   9,   1,
   2,   0,
  19,   1,
   8,   1,
   3,   0,
  19,   1,
   8,   1,
   2,   0,
  20,   1,
   7,   1,
   3,   0,
  20,   1,
   7,   1,
   2,   0,
  21,   1,
   6,   1,
   3,   0,
   1,   1,
   1,   0,
  19,   1,
   6,   1,
   2,   0,
   1,   1,
   1,   0,
  20,   1,
   5,   1,
   3,   0,
   1,   1,
   1,   0,
  20,   1,
   5,   1,
   2,   0,
  23,   1,
   5,   1,
   2,   0,
   1,   1,
   1,   0,
  21,   1,
   4,   1,
   3,   0,
  23,   1,
   4,   1,
   2,   0,
   1,   1,
   1,   0,
  22,   1,
   4,   1,
   3,   0,
  23,   1,
   3,   1,
   4,   0,
  22,   1,
   1,   0,
};

#define NUM_FRAMES 101
#define NUM_LEDS 30
Animation animation(NUM_FRAMES, animationData, NUM_LEDS);
