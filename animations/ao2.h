const PROGMEM prog_uint8_t animationData[]  = {
// Length of the color table - 1, in bytes. length: 1 byte
   2,
// Color table section. Each entry is 3 bytes. length: 9 bytes
   0,  89, 128,
 230, 230, 230,
   0, 128, 203,
// Pixel runs section. Each pixel run is 2 bytes. length: -1 bytes
   7,   2,
  23,   0,
   6,   2,
  24,   0,
   6,   2,
  24,   0,
   6,   2,
  24,   0,
   6,   2,
  24,   0,
   6,   2,
  24,   0,
   6,   2,
  24,   0,
   6,   2,
   9,   0,
   5,   1,
  10,   0,
   6,   2,
   8,   0,
   9,   1,
   7,   0,
   6,   2,
   9,   0,
   9,   1,
   6,   0,
   6,   2,
   8,   0,
  10,   1,
   6,   0,
   6,   2,
   6,   0,
  12,   1,
   6,   0,
   6,   2,
   7,   0,
  11,   1,
   6,   0,
   6,   2,
   8,   0,
  10,   1,
   6,   0,
   6,   2,
   7,   0,
  11,   1,
   6,   0,
   6,   2,
   6,   0,
  12,   1,
   6,   0,
   6,   2,
   6,   0,
  12,   1,
   6,   0,
   7,   2,
   6,   0,
  11,   1,
   6,   0,
   7,   2,
   5,   0,
  12,   1,
   6,   0,
   7,   2,
   5,   0,
  12,   1,
   6,   0,
   8,   2,
   5,   0,
  11,   1,
   6,   0,
   8,   2,
   5,   0,
  11,   1,
   6,   0,
   9,   2,
   3,   0,
  12,   1,
   6,   0,
   9,   2,
   3,   0,
  12,   1,
   6,   0,
  10,   2,
   3,   0,
  11,   1,
   6,   0,
  10,   2,
   3,   0,
  11,   1,
   6,   0,
  11,   2,
   3,   0,
  10,   1,
   6,   0,
  12,   2,
   3,   0,
   9,   1,
   6,   0,
  12,   2,
   2,   0,
  10,   1,
   6,   0,
  12,   2,
   1,   0,
  11,   1,
   6,   0,
  12,   2,
   1,   0,
  11,   1,
   6,   0,
  13,   2,
   2,   0,
   9,   1,
   6,   0,
  14,   2,
   2,   0,
   8,   1,
   6,   0,
  13,   2,
   2,   0,
   9,   1,
   6,   0,
  13,   2,
   1,   0,
  10,   1,
   6,   0,
  12,   2,
   3,   0,
   9,   1,
   6,   0,
  15,   2,
   1,   0,
   8,   1,
   1,   0,
   2,   2,
   3,   0,
  13,   2,
   1,   0,
   1,   2,
   1,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   3,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   2,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   1,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   2,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   2,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   5,   0,
   4,   1,
   1,   0,
   4,   2,
   1,   0,
  18,   2,
   1,   0,
   5,   1,
   1,   0,
   4,   2,
   1,   0,
  17,   2,
   1,   0,
   6,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   1,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  17,   2,
   1,   0,
   6,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   4,   0,
   5,   1,
   1,   0,
   4,   2,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   3,   1,
   1,   2,
   1,   0,
  17,   2,
   1,   0,
   6,   1,
   1,   0,
   4,   1,
   1,   0,
  17,   2,
   2,   0,
   5,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   1,
   1,   0,
  17,   2,
   1,   0,
   6,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   3,   0,
   5,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   2,   0,
   6,   1,
   1,   0,
   4,   1,
   1,   0,
  16,   2,
   1,   0,
   7,   1,
   1,   0,
   4,   1,
   1,   0,
  15,   2,
   2,   0,
   7,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   1,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   1,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   4,   0,
   6,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   1,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  15,   2,
   1,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   1,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   1,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   2,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   1,   0,
  10,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   2,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  14,   2,
   2,   0,
   8,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   2,   0,
   9,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   1,   0,
  10,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   1,   0,
  10,   1,
   1,   0,
   4,   2,
   1,   0,
  13,   2,
   1,   0,
  10,   1,
   1,   0,
   4,   2,
   1,   0,
  12,   2,
   1,   0,
  11,   1,
   1,   0,
   4,   2,
   1,   0,
  12,   2,
   2,   0,
  10,   1,
   1,   0,
   3,   2,
   2,   0,
  13,   2,
   1,   0,
  10,   1,
   6,   0,
  13,   2,
   1,   0,
  10,   1,
   6,   0,
  12,   2,
   1,   0,
  11,   1,
   6,   0,
  13,   2,
   1,   0,
  10,   1,
   6,   0,
  13,   2,
   1,   0,
  10,   1,
   6,   0,
  12,   2,
   2,   0,
  10,   1,
   6,   0,
  12,   2,
   1,   0,
  11,   1,
   6,   0,
  13,   2,
   2,   0,
   9,   1,
   6,   0,
  12,   2,
   3,   0,
   9,   1,
   6,   0,
  11,   2,
   3,   0,
  10,   1,
   6,   0,
  11,   2,
   3,   0,
   9,   1,
   7,   0,
  10,   2,
   6,   0,
   4,   1,
  10,   0,
   9,   2,
  21,   0,
   9,   2,
  21,   0,
   8,   2,
  22,   0,
   8,   2,
  22,   0,
   7,   2,
  23,   0,
   7,   2,
  23,   0,
   7,   2,
  23,   0,
};

#define NUM_FRAMES 101
#define NUM_LEDS 30
Animation animation(NUM_FRAMES, animationData, NUM_LEDS);
